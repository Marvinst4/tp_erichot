package com.example.api.controllers;

import com.example.api.model.Film;
import com.example.api.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
public class ApiController {

    @Autowired
    private FilmRepository filmRepository;

    @GetMapping("/films")
    public List<Film> getAllFilms() {
        return filmRepository.findAll();
    }

    @PostMapping("/post")
    public void update(@RequestBody Film film){
        filmRepository.save(film);
    }
}
