package com.example.api.model;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "film")
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "film_id")
    private Integer filmId;

    @Column(name = "title", length = 255)
    private String title;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name = "release_year")
    private Integer releaseYear;

    @Column(name = "language_id")
    private Short languageId;

    @Column(name = "rental_duration")
    private Short rentalDuration;

    @Column(name = "rental_rate", precision = 4, scale = 2)
    private BigDecimal rentalRate;

    @Column(name = "length")
    private Short length;

    @Column(name = "replacement_cost", precision = 5, scale = 2)
    private BigDecimal replacementCost;

    @Column(name = "rating")
    private String rating;  // Assuming you have a class or enum named 'MpaaRating'

    @Column(name = "last_update")
    private Timestamp lastUpdate;

    @Column(name = "fulltext", columnDefinition = "tsvector")
    private String fulltext;

    // Add getters and setters

    // Constructors, hashCode, equals, toString, etc.
}
